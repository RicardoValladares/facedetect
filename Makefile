
push:
	git status
	git add .
	git commit -m "$$(date)"
	git pull origin opensource 
	git push origin opensource

pyrun:
	python main.py

gorun:
	go run main.go

run:
	./main

compile:
	go build main.go

switch:
	git switch Neurotechnology
