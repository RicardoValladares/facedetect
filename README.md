Este proyecto es un servidor web hecho para la detección y reconocimiento de rostros; implementando un conjunto de herramientas escritas en Python, Golang, JavaScript y C++.

<p align="center">
  <img alt="Light" src="https://gitlab.com/RicardoValladares/facedetect/-/raw/opensource/desktop.png" width="70%">
  &nbsp; &nbsp;
  <img alt="Dark" src="https://gitlab.com/RicardoValladares/facedetect/-/raw/opensource/celphone.jpg" width="21%">
</p>

<br><br>

# Ejecución:

Para poder ejecutar este proyecto de manera más fácil, evitando el tedioso trabajo de instalar las dependencias para compilar, puedes ejecutar los siguientes comandos, para los cuales solo necesitarás tener instalado docker y git:

```bash
git clone https://gitlab.com/RicardoValladares/facedetect.git
cd facedetect
docker compose up
```

```bash
firefox https://localhost/identificar.html
```

# Edición:

Si quieres editar este proyecto te recomiendo visites mi repositorio en GitHub, ya que allí se encuentra escrito en Golang y Python sin hacer uso de certificado SSL para que lo puedas trabajar sin problemas en localhost y ngrok, así como también el código JavaScript y HTML de la interfaz esta sin compactar lo cual te permitirá visualizar el código de manera más clara: https://github.com/RicardoValladares/FaceDetect


<br><br>

# Dependencias:

Librerías y Modelos usadas:
- OpenCV (https://docs.opencv.org/3.4/d5/d10/tutorial_js_root.html)
- Dlib (http://dlib.net/)
- Modelos entrenados para el reconocimiento, detección y predicción de rostros: @davisking(https://github.com/davisking/dlib-models)


### Instalar dependencias en FreeBSD
Si eres usuario del sistema operativo FreeBSD puedes instalar las dependencias ejecutando los siguientes comandos: 

```bash
pkg install python39 py39-Flask py39-dlib
pkg install graphics/py-face_recognition
```

### Instalar dependencias en Linux o MacOS
En Linux y MacOS antes de ejecutar los siguientes comandos, es necesario instalar Python y PyPI.

```bash
pip install Flask
pip install Flask-BasicAuth
pip install dlib
pip install face_recognition
pip install face_recognition_models
```

### En Windows no es oficialmente compatible, pero puede funcionar:
En el siguiente enlace encontraras como instalar las dependencias de python-3.9.0 en Windows si quieres editar el codigo: https://www.geeksforgeeks.org/how-to-install-dlib-library-for-python-in-windows-10/

<br><br>

# Certificado de Host Virtual en una Intranet de IP Estática:

### Instalar ssl en FreeBSD
Para instalar el certificado en las computadoras clientes FreeBSD: 

- Verificar conexión a la IP del servidor desde el cliente haciendo Ping a dicha IP desde Terminal.
- Editar el archivo usando el comando: "sudo nano /etc/hosts", agregando una línea de código que defina: "IP HOST" un ejemplo seria: "192.168.0.7 facedetect.com".
- Ejecutar los siguientes comandos:

```bash
sudo chmod a+x facedetect.com.crt
sudo cp facedetect.com.crt /usr/local/etc/ssl/facedetect.pem
sudo certctl rehash - it
```

### Instalar ssl en Ubuntu
Para instalar el certificado en las computadoras clientes derivadas de Ubuntu: 

- Verificar conexión a la IP del servidor desde el cliente haciendo Ping a dicha IP desde Terminal.
- Editar el archivo usando el comando: "sudo nano /etc/hosts", agregando una línea de código que defina: "IP HOST" un ejemplo seria: "192.168.0.7 facedetect.com".
- Ejecutar los siguientes comandos:

```bash
sudo chmod a+x facedetect.com.crt
sudo cp facedetect.com.crt /usr/local/share/ca-certificates/facedetect.com.crt
sudo update-ca-certificates 
```

### Instalar ssl en ArchLinux
Para instalar el certificado en las computadoras clientes ArchLinux: 

- Verificar conexión a la IP del servidor desde el cliente haciendo Ping a dicha IP desde Terminal.
- Editar el archivo usando el comando: "sudo nano /etc/hosts", agregando una línea de código que defina: "IP HOST" un ejemplo seria: "192.168.0.7 facedetect.com".
- Ejecutar los siguientes comandos:

```bash
sudo chmod a+x facedetect.com.crt
sudo cp facedetect.com.crt /usr/share/ca-certificates/trust-source/anchors/facedetect.com.crt
sudo update-ca-trust extract 
```

### Instalar ssl en Windows
Para instalar el certificado en las computadoras clientes Windows: 

- Verificar conexión a la IP del servidor desde el cliente haciendo Ping a dicha IP desde CMD.
- Editar el archivo: "C:\Windows\System32\drivers\etc\hosts" con block de notas, agregándole una línea de código que defina: "IP HOST" un ejemplo seria: "192.168.0.7 facedetect.com".
- Ejecutar el archivo "facedetect.com.crt" y seguir las indicaciones de la imagen:

<img alt="Light" src="https://gitlab.com/RicardoValladares/facedetect/-/raw/opensource/wincert.png" width="100%">


### Instalar ssl en Android
Para instalar el certificado en las computadoras, tablets y telefonos clientes Android: 

- Verificar conexión a la IP del servidor desde el cliente haciendo Ping a dicha IP desde Termux.
- Editar el archivo: "./android/host.txt" con un editor de texto, por defecto contiene: "192.168.0.7 facedetect.com", hay que colocar la ip del servidor.
- Asegurarse que el servidor se este ejecutando antes de seguir los pasos indicados en el video que contiene de hipervinculo la imagen siguiente:

[![Instrucciones para Android](https://gitlab.com/RicardoValladares/facedetect/-/raw/opensource/androcert.gif)](https://youtu.be/6RNnN5sRVYg)


