module gitlab.com/RicardoValladares/facedetect

go 1.17

require github.com/leandroveronezi/go-recognizer v1.0.0

require (
	github.com/Kagami/go-face v0.0.0-20210630145111-0c14797b4d0e // indirect
	github.com/disintegration/imaging v1.6.2 // indirect
	github.com/fogleman/gg v1.3.0 // indirect
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/kabukky/httpscerts v0.0.0-20150320125433-617593d7dcb3
	golang.org/x/image v0.0.0-20200119044424-58c23975cae1 // indirect
)
