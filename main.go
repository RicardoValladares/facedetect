package main

import (
	"crypto/sha256"
	"crypto/subtle"
	"encoding/base64"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"

	"github.com/kabukky/httpscerts"
	"github.com/leandroveronezi/go-recognizer"
)

const (
	host     = "facedetect.com"
	user     = "admin"
	pass     = "admin"
	fotosDir = "enrrolados"
	dataDir  = "modelos"
)

var rec = recognizer.Recognizer{}

func addFile(rec *recognizer.Recognizer, Path, Id string) error {
	err := rec.AddImageToDataset(Path, Id)
	return err
}

func hasher(s string) []byte {
	val := sha256.Sum256([]byte(s))
	return val[:]
}

func authBasicHandler(handler http.HandlerFunc, userhash, passhash []byte, realm string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		user, pass, ok := r.BasicAuth()
		if !ok || subtle.ConstantTimeCompare(hasher(user), userhash) != 1 || subtle.ConstantTimeCompare(hasher(pass), passhash) != 1 {
			w.Header().Set("WWW-Authenticate", `Basic realm="`+realm+`"`)
			http.Error(w, "Necesita credenciales para poder enrrolar... Para Identificar: ("+r.Host+"/identificar.html)", http.StatusUnauthorized)
			return
		}
		handler(w, r)
	}
}

func main() {
	var userhash = hasher(user)
	var passhash = hasher(pass)
	err := httpscerts.Check(host+".crt", host+".key")
	if err != nil {
		err = httpscerts.Generate(host+".crt", host+".key", host)
		if err != nil {
			log.Fatal("Error No se lograron crear los certificados https.")
			return
		}
	}
	err = rec.Init(dataDir)
	if err != nil {
		log.Println("Error al inicializar biometria con los templates")
		return
	}
	rec.Tolerance = 0.4
	rec.UseGray = true
	rec.UseCNN = false
	defer rec.Close()
	files, err := ioutil.ReadDir("./enrrolados/")
	if err != nil {
		log.Println("Error al inicializar biometria con los rostros enrrolados")
		return
	}
	fmt.Println("Cargando rostros enrrolados...")
	for _, file := range files {
		if addFile(&rec, filepath.Join(fotosDir, file.Name()), file.Name()) != nil {
			log.Println("Imagen no valida:", file.Name())
			return
		} else {
			log.Println("Imagen cargada:", file.Name())
		}
	}
	rec.SetSamples()
	url := "https://" + host
	fmt.Println(url)
	fmt.Println("https://localhost")
	http.HandleFunc("/", authBasicHandler(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "<html oncontextmenu='return false' onkeydown='return false'><head><meta name='viewport' content='width=device-width, initial-scale=1.0'> <title>Enrrolar</title><script async src='opencv.js' onload='openCvReady();'></script><script src='utils.js'></script><style> .labele { color: white; padding: 8px; font-family: Arial; background-color: #ff9800; } .labelt { color: white; padding: 8px; font-family: Arial; background-color: #04aa6d; } @media only screen and (max-width: 992px) { video.camara { height:640px; width:480px; display: block; margin-left: auto; margin-right: auto; } } @media only screen and (min-width: 993px) { video.camara { height:480px; width:640px; display: block; margin-left: auto; margin-right: auto;} } </style></head><body  bgcolor='#000' onload=\"setTimeout('temporizador()',1000)\"> <br><center> <span id='Tiempo' class='labelt'>0</span> <span id='Estado' class='labele'>Iniciando...</span> </center><br><br><center><! canvas id='canvas_output' /><! /canvas> <video id='cam_input' height='480' width='640' class='camara'></video> </center><script>/* variables globales para el funcionamiento */let tiempo = 0; let stop = 0; /* aperturamos webcam con opencv */function openCvReady() {cv['onRuntimeInitialized'] = () => {let video = document.getElementById('cam_input');/*video.style.display='none';*/ navigator.mediaDevices.getUserMedia({ video: true, audio: false }).then(function (stream) { video.srcObject = stream; video.play(); }).catch(function (err) { console.log('Error: ' + err); });let src = new cv.Mat(video.height, video.width, cv.CV_8UC4);let gray = new cv.Mat();let cap = new cv.VideoCapture(cam_input);let faces = new cv.RectVector();let faceClassifier = new cv.CascadeClassifier();let utils = new Utils('errorMessage');let faceCascade = 'haarcascade_frontalface_default.xml';utils.createFileFromUrl(faceCascade, faceCascade, () => { faceClassifier.load(faceCascade); });const FPS = 40;function processVideo() {let begin = Date.now();cap.read(src);cv.cvtColor(src, gray, cv.COLOR_RGBA2GRAY, 0);let detectado=0;try {faceClassifier.detectMultiScale(gray, faces, 1.1, 3, 0);for (let i = 0; i < faces.size(); ++i) {let face = faces.get(i);let point1 = new cv.Point(face.x, face.y);let point2 = new cv.Point(face.x + face.width, face.y + face.height);cv.rectangle(src, point1, point2, [0, 255, 0, 255]);detectado = 1;}} catch (err) {console.log(err);}if(detectado==1){document.getElementById('Estado').innerHTML = 'Rostro detectado';document.getElementById('Estado').style.backgroundColor='#04aa6d';} else{document.getElementById('Estado').innerHTML = 'Rostro no detectado';document.getElementById('Estado').style.backgroundColor='#f44336';}/*cv.imshow('canvas_output', src);*/let delay = 1000 / FPS - (Date.now() - begin);setTimeout(processVideo, delay);} setTimeout(processVideo, 0);}}/* temporizador que usa la variable global tiempo para contar los segundos */function temporizador() {if(stop==0){if(document.getElementById('Estado').textContent=='Rostro detectado'){tiempo = tiempo + 1;document.getElementById('Tiempo').innerHTML = tiempo;} else{tiempo = 0;document.getElementById('Tiempo').innerHTML = tiempo;}/*cuando haya pasado 3 segundos de la deteccion de un rostro ejecutar()*/if(tiempo == 3){stop = 1;tiempo = 0;document.getElementById('Tiempo').innerHTML = tiempo;ejecutar(); }}setTimeout('temporizador()',1000);}/* generamos el archivo de imagen sin el recuadro */function ejecutar(){let imageCanvas = document.createElement('canvas');let imageCtx = imageCanvas.getContext('2d');let v = document.getElementById('cam_input');imageCanvas.width = v.videoWidth;imageCanvas.height = v.videoHeight;imageCtx.drawImage(v, 0, 0, v.videoWidth, v.videoHeight);imageCanvas.toBlob(postFile, 'image/jpeg');}/* enviamos el 'file' y el 'identificador' a la url 'enrrolar' por metodo 'POST' */function postFile(file) {let formdata = new FormData();let identificador = prompt('Ingrese un identificador:');if(identificador==null){stop = 0;return } formdata.append('id', identificador);formdata.append('image', file);let xhr = new XMLHttpRequest();xhr.open('POST', 'enrrolar', true); xhr.setRequestHeader('Authorization', 'Basic "+base64.StdEncoding.EncodeToString([]byte(user+":"+pass))+"'); xhr.onload = function () {if (this.status === 200){alert(this.response); stop = 0;}else{alert(this.response); stop = 0;}};xhr.onerror = function () {alert('Error de comunicacion con el servidor');stop = 0;};xhr.onabort = function () {alert('Peticion de reconocimiento abortada');stop = 0;};xhr.send(formdata);}</script></body></html>")
	}, userhash, passhash, "BasicAuth necesita credenciales"))
	http.HandleFunc("/identificar.html", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "<html oncontextmenu='return false' onkeydown='return false'><head><meta name='viewport' content='width=device-width, initial-scale=1.0'> <title>Identificar</title><script async src='opencv.js' onload='openCvReady();'></script><script src='utils.js'></script><style> .labele { color: white; padding: 8px; font-family: Arial; background-color: #ff9800; } .labelt { color: white; padding: 8px; font-family: Arial; background-color: #04aa6d; } .labeli { color: white; padding: 8px; font-family: Arial; background-color: #f44336; } @media only screen and (max-width: 992px) { video.camara { height:640px; width:480px; display: block; margin-left: auto; margin-right: auto; } } @media only screen and (min-width: 993px) { video.camara { height:480px; width:640px; display: block; margin-left: auto; margin-right: auto;} } </style></head><body  bgcolor='#000' onload=\"setTimeout('temporizador()',1000)\"> <br><center> <span id='Tiempo' class='labelt'>0</span> <span id='Estado' class='labele'>Iniciando...</span> <span id='Identificador' class='labeli'>Desconocido</span> </center><br><br><center><! canvas id='canvas_output' /><! /canvas> <video id='cam_input' height='480' width='640' class='camara'></video> </center><script>/* variables globales para el funcionamiento */let stop = 0; let tiempo = 0; /* aperturamos webcam con opencv */function openCvReady() {cv['onRuntimeInitialized'] = () => {let video = document.getElementById('cam_input');/*video.style.display='none';*/ navigator.mediaDevices.getUserMedia({ video: true, audio: false }).then(function (stream) { video.srcObject = stream; video.play(); }).catch(function (err) { console.log('Error: ' + err); });let src = new cv.Mat(video.height, video.width, cv.CV_8UC4);let gray = new cv.Mat();let cap = new cv.VideoCapture(cam_input);let faces = new cv.RectVector();let faceClassifier = new cv.CascadeClassifier();let utils = new Utils('errorMessage');let faceCascade = 'haarcascade_frontalface_default.xml';utils.createFileFromUrl(faceCascade, faceCascade, () => { faceClassifier.load(faceCascade); });const FPS = 40;function processVideo() {let begin = Date.now();cap.read(src);cv.cvtColor(src, gray, cv.COLOR_RGBA2GRAY, 0);let detectado=0;try {faceClassifier.detectMultiScale(gray, faces, 1.1, 3, 0);for (let i = 0; i < faces.size(); ++i) {let face = faces.get(i);let point1 = new cv.Point(face.x, face.y);let point2 = new cv.Point(face.x + face.width, face.y + face.height);cv.rectangle(src, point1, point2, [0, 255, 0, 255]);detectado = 1;}} catch (err) {console.log(err);}if(detectado==1){document.getElementById('Estado').innerHTML = 'Rostro detectado';document.getElementById('Estado').style.backgroundColor='#04aa6d';} else{document.getElementById('Estado').innerHTML = 'Rostro no detectado';document.getElementById('Estado').style.backgroundColor='#f44336';}/*cv.imshow('canvas_output', src);*/let delay = 1000 / FPS - (Date.now() - begin);setTimeout(processVideo, delay);} setTimeout(processVideo, 0);}}/* temporizador que usa la variable global tiempo para contar los segundos */function temporizador() {if(stop==0){if(document.getElementById('Estado').textContent=='Rostro detectado'){tiempo = tiempo + 1;document.getElementById('Tiempo').innerHTML = tiempo;} else{tiempo = 0; document.getElementById('Tiempo').innerHTML = tiempo;}/*cuando hayan pasado 3 segundos de la deteccion de un rostro ejecutar()*/if(tiempo == 3){stop = 1; tiempo = 0; document.getElementById('Tiempo').innerHTML = tiempo;document.getElementById('Identificador').innerHTML = 'Enviando al servidor';document.getElementById('Identificador').style.backgroundColor='#ff9800';ejecutar(); }}setTimeout('temporizador()',1000); }/* generamos el archivo de imagen sin el recuadro */function ejecutar(){let imageCanvas = document.createElement('canvas');let imageCtx = imageCanvas.getContext('2d');let v = document.getElementById('cam_input');imageCanvas.width = v.videoWidth;imageCanvas.height = v.videoHeight;imageCtx.drawImage(v, 0, 0, v.videoWidth, v.videoHeight);imageCanvas.toBlob(postFile, 'image/jpeg');}/* enviamos el 'file' a la url 'identificar' por metodo 'POST' */function postFile(file) {let formdata = new FormData();formdata.append('image', file);let xhr = new XMLHttpRequest();xhr.open('POST', 'identificar', true);xhr.onload = function () {/*si se hizo un envio exitoso, sin error; enviamos la respuerta indentificacion()*/if (this.status === 200){if (this.response != 'NO IDENTIFICADO'){ identificacion(this.response); }else{ document.getElementById('Identificador').innerHTML = 'NO IDENTIFICADO';document.getElementById('Identificador').style.backgroundColor='#f44336';stop = 0; }} else {document.getElementById('Identificador').innerHTML = 'NO IDENTIFICADO';document.getElementById('Identificador').style.backgroundColor='#f44336';stop = 0;}};xhr.onerror = function () {document.getElementById('Identificador').innerHTML = 'Error de comunicacion';alert('Error de comunicacion con el servidor');document.getElementById('Identificador').innerHTML = 'NO IDENTIFICADO';document.getElementById('Identificador').style.backgroundColor='#f44336';stop = 0;};xhr.onabort = function () {document.getElementById('Identificador').innerHTML = 'Peticion abortada';alert('Peticion de reconocimiento abortada');document.getElementById('Identificador').innerHTML = 'NO IDENTIFICADO';document.getElementById('Identificador').style.backgroundColor='#f44336';stop = 0;};xhr.send(formdata);}/* mostramos el Identificador de la persona por 5 segundos */async function identificacion(res) {document.getElementById('Identificador').innerHTML = res;document.getElementById('Identificador').style.backgroundColor='#00008b';await sleep(5 * 1000); document.getElementById('Identificador').innerHTML = 'Desconocido';document.getElementById('Identificador').style.backgroundColor='#f44336';stop = 0;}/* funcion que simula sleep */function sleep(ms) {return new Promise((resolve) => setTimeout(resolve, ms));}</script></body></html>")
	})
	http.HandleFunc("/opencv.js", func(w http.ResponseWriter, r *http.Request) { http.ServeFile(w, r, "opencv.js") })
	http.HandleFunc("/utils.js", func(w http.ResponseWriter, r *http.Request) { http.ServeFile(w, r, "utils.js") })
	http.HandleFunc("/haarcascade_frontalface_default.xml", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "haarcascade_frontalface_default.xml")
	})
	http.Handle("/enrrolados/", http.StripPrefix("/enrrolados", (http.FileServer(http.Dir("./enrrolados")))))
	http.Handle("/android/", http.StripPrefix("/android", (http.FileServer(http.Dir("./android")))))
	http.HandleFunc("/enrrolar", enrrolar)
	http.HandleFunc("/identificar", identificar)
	err = http.ListenAndServeTLS(":443", host+".crt", host+".key", nil)
	if err != nil {
		log.Fatal("Error de ListenAndServeTLS, Necesites derechos de super usuario y que el puerto 443 este desocupado sin bloqueos del firewall")
	}
}

func identificar(w http.ResponseWriter, r *http.Request) {
	if r.Header.Get("origin") != ("https://"+host) && r.Header.Get("origin") != "http://localhost" {
		http.Error(w, "Error, el origen es invalido", http.StatusBadRequest)
		log.Println("Error, el origen es invalido")
		return
	}
	file, handler, err := r.FormFile("image")
	if err != nil {
		http.Error(w, "Error al obtener la imagen", http.StatusBadRequest)
		log.Println("Error al obtener la imagen")
		file.Close()
		return
	}
	defer file.Close()
	tempFile, err := ioutil.TempFile("temp", "*"+filepath.Ext(handler.Filename))
	if err != nil {
		http.Error(w, "Error al nombrar la imagen", http.StatusBadRequest)
		log.Println("Error al nombrar la imagen")
		file.Close()
		tempFile.Close()
		return
	}
	defer tempFile.Close()
	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		http.Error(w, "Error al leer la imagen", http.StatusBadRequest)
		log.Println("Error al leer la imagen")
		file.Close()
		tempFile.Close()
		return
	}
	tempFile.Write(fileBytes)
	filetype := http.DetectContentType(fileBytes)
	if filetype != "image/jpeg" && filetype != "image/jpg" && filetype != "image/gif" && filetype != "image/png" {
		http.Error(w, "Error de formato en el archivo", http.StatusBadRequest)
		log.Println("Error de formato en el archivo")
		file.Close()
		tempFile.Close()
		return
	}
	face, err := rec.ClassifyMultiples(tempFile.Name())
	if err != nil {
		http.Error(w, "Error al comparar rostro", http.StatusBadRequest)
		log.Println("Error al comparar rostro")
		file.Close()
		tempFile.Close()
		return
	}

	if len(face) > 0 {
		fmt.Fprintf(w, face[0].Data.Id)
		log.Println(face[0].Data.Id)
	} else {
		fmt.Fprintf(w, "NO IDENTIFICADO")
		log.Println("NO IDENTIFICADO")
	}
	file.Close()
	tempFile.Close()
}

func enrrolar(w http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Authorization") != ("Basic " + base64.StdEncoding.EncodeToString([]byte(user+":"+pass))) {
		http.Error(w, "Error, las credenciales no son validas", http.StatusBadRequest)
		log.Println("Error, las credenciales no son validas")
		return
	}
	if r.Header.Get("origin") != ("https://"+host) && r.Header.Get("origin") != "http://localhost" {
		http.Error(w, "Error, el origen es invalido", http.StatusBadRequest)
		log.Println("Error, el origen es invalido")
		return
	}
	identificador := r.FormValue("id")
	enrrolados, err := os.Stat("./enrrolados/" + identificador)
	if (err == nil) && !enrrolados.IsDir() {
		fmt.Fprintf(w, "Ya existe el identificador: '%s'\n", identificador)
		log.Println("Ya existe el identificador:", identificador)
		return
	}
	if identificador == "" || identificador == "null" {
		http.Error(w, "Error al obtener el identificador", http.StatusBadRequest)
		log.Println("Error al obtener el identificador")
		return
	}
	file, handler, err := r.FormFile("image")
	if err != nil {
		http.Error(w, "Error al obtener el imagen", http.StatusBadRequest)
		log.Println("Error al obtener el imagen")
		file.Close()
		return
	}
	defer file.Close()
	filetype := handler.Header.Get("content-type")
	if filetype != "image/jpeg" && filetype != "image/jpg" && filetype != "image/gif" && filetype != "image/png" {
		http.Error(w, "Error de formato en el archivo", http.StatusBadRequest)
		log.Println("Error de formato en el archivo")
		file.Close()
		return
	}
	dst, err := os.Create("./enrrolados/" + identificador)
	if err != nil {
		http.Error(w, "Error al enrrolar imagen", http.StatusBadRequest)
		log.Println("Error al enrrolar imagen")
		file.Close()
		dst.Close()
		return
	}
	defer dst.Close()
	_, err = io.Copy(dst, file)
	if err != nil {
		http.Error(w, "Error al almacenar la imagen", http.StatusBadRequest)
		log.Println("Error al almacenar la imagen")
		file.Close()
		dst.Close()
		return
	}
	file.Close()
	dst.Close()
	addFile(&rec, filepath.Join(fotosDir, identificador), identificador)
	rec.SetSamples()
	fmt.Fprintf(w, "Enrrolado Exitosamente '%s'\n", identificador)
	log.Println("Enrrolado Exitosamente ", identificador)
}
